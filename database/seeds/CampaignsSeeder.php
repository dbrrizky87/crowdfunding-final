<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CampaignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $campaigns = [];
        $faker = Faker\Factory::create();
        for ($i=0; $i < 5 ; $i++) { 
            $avatar_path ='public/images';
            $avatar_fullpath = $faker->image($avatar_path,200, 250, 'people', true , true , 'people');
            $avatar_image =explode("\\", $avatar_fullpath) ;
            $campaigns[$i] = [
                'id' => $faker->uuid(20),
                'title' => $faker->sentence,
                'image' => 'images/'. $avatar_image[1],
                'description' => $faker->text,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now()
            ];
        }
        DB::table('campaigns')->insert( $campaigns);
        // factory(App\Campaigns::class, 5)->create();

    }
}
