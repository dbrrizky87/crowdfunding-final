<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [];
        $faker = Faker\Factory::create();
        for ($i=0; $i <= 2 ; $i++) { 
            $avatar_path ='public/images/users';
            $avatar_fullpath = $faker->image($avatar_path,250, 200, 'people', true , true , 'people');
            $avatar_image =explode("\\", $avatar_fullpath) ;
            $blogs[$i] = [
                'id' => $faker->uuid(20),
                'name' => $faker->name,
                'email' => $faker->email,
                'avatar' => 'images/'. $avatar_image[1],
                'description' => $faker->text,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now()
            ];
        }
        DB::table('users')->insert($users);
    }
}
