<?php

use Illuminate\Database\Seeder;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         
        $blogs = [];
        $faker = Faker\Factory::create();
        for ($i=0; $i <= 5 ; $i++) { 
            $avatar_path ='public/images/blogs';
            $avatar_fullpath = $faker->image($avatar_path,250, 200, 'people', true , true , 'people');
            $avatar_image =explode("\\", $avatar_fullpath) ;
            $blogs[$i] = [
                'id' => $faker->uuid(20),
                'title' => $faker->sentence,
                'image' => 'images/'. $avatar_image[1],
                'description' => $faker->text,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now()
            ];
        }
        DB::table('blogs')->insert($blogs);
        // factory(App\blogs::class, 5)->create();
    }
}
