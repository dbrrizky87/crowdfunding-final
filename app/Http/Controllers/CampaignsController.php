<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;

class CampaignsController extends Controller
{

    public function index(){
        
        $campaigns = Campaign::paginate(3);
        $data['campaigns'] = $campaigns;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'data campaigns berhasil ditampilan',
            'data' => $data
        ], 200);


    }

    public function random($count){
        $campaigns = Campaign::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'data campaigns berhasil ditampilan',
            'data' => $data
        ], 200);
    }


    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        if ($files = $request->file('image')) {
            // Define upload path
            $destinationPath = public_path('/images/'); // upload path
            // Upload Orginal Image           
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
 
            $insert['image'] = "$profileImage";
            // Save In Database
             $campaign= new Campaign();
             $campaign->title = $request->name;
             $campaign->description = $request->description;
             $campaign->image="$profileImage";
             $campaign->save();
             return back()->with('success', 'Image Upload successfully');
         }
    }

    public function detail($id){
        
        $campaign = Campaign::find($id);
     
        $data['campaign'] = $campaign;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function search($keyword){
        
        $campaigns = Campaign::select('*')
        ->where('title', 'LIKE', "%".$keyword."%")
        ->get();
     
        $data['campaigns'] = $campaigns;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }
}

