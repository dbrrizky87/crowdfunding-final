<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogsController extends Controller
{
    public function index(){
        
        $blogs = Blog::select('*');
        return $blogs;
    }

    public function random($count){
        $blogs = Blog::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();

        $data['blogs'] = $blogs;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'data Blogs berhasil ditampilan',
            'data' => $data
        ], 200);
    }


    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        if ($files = $request->file('image')) {
            // Define upload path
            $destinationPath = public_path('/images/'); // upload path
            // Upload Orginal Image           
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
 
            $insert['image'] = "$profileImage";
            // Save In Database
             $Blog= new Blog();
             $Blog->title = $request->name;
             $Blog->description = $request->description;
             $Blog->image="$profileImage";
             $Blog->save();
             return back()->with('success', 'Image Upload successfully');
         }
    }

    public function show($id){
        
        // $Blogs = Blog::find($id)-get();
        return view('Blogs/{id}', ['Blog' => Blog::findOrFail($id)]);
    }
}
