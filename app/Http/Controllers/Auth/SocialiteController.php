<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider){
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }


    public function handleProviderCallback($provider){
        try{
        $social_user = Socialite::driver($provider)->stateless()->user();
    
        if(!$social_user){
            
        return response([
            'response_code' => '01',
            'response_message' => 'social - user data is empty',

        ],401);
        }
        $user = User::whereEmail($social_user->email)->first();

        if(!$user){
            
            $user = User::create([
                'email' => $social_user->email,
                'name' => $social_user->name,
                'email_verified_at' => Carbon::now(),
               
            ]);
        } 
        $data['user'] = $user;
        $data['token'] = auth()->login($user);

    return response([
        'response_code' => '00',
        'response_message' => 'Login Successfully',
        'data' => $data
    ], 200);
    
    } catch(\Throwable $th) 
        {

        return response([
            'response_code' => '01',
            'response_message' => 'Login Failed',
    
        ],401);
        }
   
    }
}
