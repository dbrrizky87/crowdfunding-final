<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([

            'name' => ['required' ],
            'email' => ['email','required','unique:users,email' ],
            'password' => ['required','min:6' ]
        ]);

       $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);
        
        $data['user'] =$user;

        return response()->json([
            'respon_code' => '00',
            'respon_message' => 'Thanks, You are registered',
            'data' => $data
        ], 200);
    }
}
