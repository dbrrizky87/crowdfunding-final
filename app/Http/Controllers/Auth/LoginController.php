<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
    
        $credentials= $request->only('email','password');
        
        if(!$token = auth()->attempt($credentials)){
            return response(['error' => 'email or password did not match'],401);
        }
        
        $data['token'] = $token;
        $data['user'] = auth()->user();

        return response([
            'respon_code' => '00',
            'respon_message' => 'Login Success',
            'data' => $data
        ],200);
    }
}
