<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'prefix'     => 'auth',
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController')->middleware('auth:api');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');

    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');


}); 

// Route::namespace('Auth')->group(function(){
//     Route::post('register', 'RegisterController');
//     Route::post('login', 'LoginController');
//     Route::post('logout', 'LogoutController');

// });

Route::get('user', 'UserController');


Route::group([
    'middleware' => 'api',
    'prefix'     => 'campaign',
], function(){
    Route::get('/', 'CampaignsController@index');
    Route::get('random/{count}', 'CampaignsController@random');
    Route::post('create', 'CampaignsController@store');
    Route::get('/{id}', 'CampaignsController@detail');
    Route::get('/search/{keyword}', 'CampaignsController@search');


}); 

// Route::get('campaign/{id}', 'CampaignsController@detail');

Route::group([
    'middleware' => 'api',
    'prefix'     => 'blogs',
], function(){
    
    Route::get('random/{count}', 'BlogsController@random');
    Route::post('create', 'BlogsController@store');
    

    
}); 


