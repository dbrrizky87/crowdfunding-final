<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{any?}', 'app')->where('any', '.*');
// Route::get('/login', function(){
//     return view ('welcome');
// });
// Route::get('campaigns/', 'CampaignsController@index');
// Route::get('random/{count}', 'CampaignsController@random');
// Route::get('campaign/{id}', 'CampaignsController@detail');
// Route::post('create', 'CampaignsController@store');


// Route::group([
//     'prefix'     => 'campaign',
// ], function(){
//     Route::get('/', 'CampaignsController@index');
//     Route::get('random/{count}', 'CampaignsController@random');
//     Route::post('create', 'CampaignsController@store');
//     Route::get('/{id}', 'CampaignsController@detail');

// }); 


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
